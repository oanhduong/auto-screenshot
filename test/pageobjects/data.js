const lineReader = require('line-reader')
const Promise = require('bluebird')
const dataFiles = {
    cookies: 'cookies.txt',
    domains: 'domains.txt',
    accounts: 'accounts.txt'
}

const readLines = async (file) => {
    const arr = []
    const eachLine = Promise.promisify(lineReader.eachLine)
    await eachLine(file, (line) => arr.push(line))
    return arr.map((e) => String(e).trim()).filter((e) => e)
}

const filterAccounts = async () => {
    const allDomains = await readLines(dataFiles.domains)
    const allAccs = (await readLines(dataFiles.accounts))
        .map((acc) => {
            const split = acc.split('|')
            return {
                uid: String(split[0]).trim(),
                email: split[1] ? String(split[1]).trim() : '',
                name: split[2] ? String(split[2]).trim() : '',
                raw: acc
            }
        })
        .filter((acc) => acc.uid && acc.email && acc.name && allDomains.includes(acc.email.split('@')[1]))
    return allAccs
}

const parseCookies = async () => {
    const all = await readLines(dataFiles.cookies)
    return all.map((line) => {
        const split = line.split(';')
        const cookie = []
        split.forEach((keyVal) => {
            let [key, val] = String(keyVal).trim().split('=')
            if (key && val && String(key).trim() && String(val).trim()) {
                cookie.push({
                    name: String(key).trim(),
                    value: String(val).trim()
                })
            }
        })
        return cookie
    })
}

module.exports = {filterAccounts, parseCookies}
