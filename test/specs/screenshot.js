const data = require('../pageobjects/data')
const path = require('path')

const timeoutPromise = (time) => new Promise((res) => setTimeout(() => res(), time))
const isExisting = async (el) => {
  const elem = await $(el)
  return await elem.isExisting()
}

const setCookies = async (cookies) => {
  if (cookies.length <= 0) {
    throw new Error('Cookies are empty')
  }
  await browser.deleteAllCookies()
  await browser.url('/')
  const cookie = cookies.shift()
  await browser.setCookies(cookie)
  return cookie
}

describe('Facebook Automation', () => {

  const usedCookies = []
  const processedAcc = []

  before(() => {
    browser.addCommand('takeFullPageScreenshot', function (options = {}) {
      return browser.call(async () => {
        const puppeteer = await browser.getPuppeteer()
        const pages = await puppeteer.pages()
        return pages[0].screenshot({ ...options, fullPage: true })
      })
    })
  })

  it('', async () => {
    const acc = await data.filterAccounts()
    const cookies = await data.parseCookies()

    usedCookies.push(await setCookies(cookies))

    while (acc.length > 0) {
      try {
        const ac = acc.shift()
        await browser.url(`/${ac.uid}`)
        await timeoutPromise(1000)
        if (await isExisting(`a[aria-label="${ac.name}"]`)) {
          //await browser.takeFullPageScreenshot({ path: path.join('screenshots',`${ac.raw.replace(/\|/g,'_').replace(/\//g,'-')}.png`) })
          await browser.saveScreenshot(path.join('screenshots', `${ac.raw.replace(/\|/g, '_').replace(/\//g, '-')}.png`))
          processedAcc.push(ac)
        } else {
          usedCookies.push(await setCookies(cookies))
          acc.push(ac)
        }   
      } catch (error) {
        // skip
      }
    }
  })

  after(() => {
    const fs = require('fs')
    fs.writeFileSync('report.json', JSON.stringify({
      usedCookies, processedAcc
    }))
  })
})
